//const { testObject } = require("./inventory/inventory")

//const object2={location:"Gotham",time:"Night"}
module.exports={
    defaults: function(obj,defaultProps){
        for(let key in defaultProps){
            if(!obj[key])obj[key]=defaultProps[key];
        }
        return obj;
    }
}