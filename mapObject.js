//const inventory = require("./inventory/inventory")

module.exports={
    mapObject: function(obj,cb){
        let mapped={}
        for(let value in obj){
            mapped[value]=cb(obj[value])
        }
        return mapped
}
}